var mApp;

(:test)
function startWatch(logger) {

    logger.debug("Creating App");
    mApp = new CrystalApp;

    logger.debug("Initializing App");
    mApp.initialize();

    logger.debug("Starting App");
    mApp.onStart();

    logger.debug("Getting Initial View");
    var View = mApp.getInitialView();

	return 1;
}
